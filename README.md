# lib_up7_proxy

# What is it?

It is clone of proxy lib from [μP7 (micro P7)](http://baical.net/up7.html) library that lightweight C library for 
sending trace/logs & telemetry data from your Micro-controller’s 
firmware to host/HW FIFO/cycle buffer/network/etc. 
